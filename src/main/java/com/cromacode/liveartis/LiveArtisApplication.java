package com.cromacode.liveartis;

import com.cromacode.liveartis.config.SwaggerConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(SwaggerConfiguration.class)
public class LiveArtisApplication {

    public static void main(String[] args) {
        SpringApplication.run(LiveArtisApplication.class, args);
    }
}
